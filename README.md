# Sing-a-Song_BE

-fastApi application for Sing-a-Song backend

## Local development with virtual enviroment

1. Clone repository
2. Create virtual enviroment in root directory:
   ```
   python3 -m venv myenv
   ```
3. Activate virtual enviroment
   ```
   source myenv/bin/activate
   ```
4. Install requirements:
   ```
   pip install -r requirements.txt
   ```
5. cd into the **app** folder:
6. Start the application:
   ```
   uvicorn main:app --reload
   ```
7. App: **localhost:8000**
8. Docs: **localhost:8000/docs**
