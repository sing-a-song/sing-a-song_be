def get_pagination(end: int, page_num: int, page_size: int, data_length: int, search_query: str):
    pagination = {
        'next': None,
        'previous': None
    }
    
    if end >= data_length:
        pagination["next"] = None
        if page_num > 1:
            if search_query:
                if data_length <= page_size:
                    pagination["previous"] = None
                else:
                    pagination["previous"] = f"/songs?page_num={page_num-1}&page_size={page_size}&search_query={search_query}"
            else:
                pagination["previous"] = f"/songs?page_num={page_num-1}&page_size={page_size}"
        else:
            pagination["previous"] = None
    else:
        if page_num > 1:
            if search_query:
                pagination["previous"] = f"/songs?page_num={page_num-1}&page_size={page_size}&search_query={search_query}"
            else:
                pagination["previous"] = f"/songs?page_num={page_num-1}&page_size={page_size}"
        else:
            pagination["previous"] = None

        if search_query:
            pagination["next"] = f"/songs?page_num={page_num+1}&page_size={page_size}&search_query={search_query}"
        else:
            pagination["next"] = f"/songs?page_num={page_num+1}&page_size={page_size}"

    return pagination