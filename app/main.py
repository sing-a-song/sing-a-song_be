from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from routers import users, songs, comments
from contextlib import asynccontextmanager
from db.database import Database
import logging


logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(name)s:%(message)s',
    level=logging.INFO
    )


db = Database()


@asynccontextmanager
async def lifespan(app: FastAPI):
    logging.info("Connecting to database...")
    await db.create_connection()
    yield
    logging.info("Closing database connection...")
    await db.close_connection()


app = FastAPI(lifespan=lifespan, title="Sing-a-song API")


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.middleware("http")
async def db_middleware(request: Request, call_next):
    request.state.db = db.connection
    response = await call_next(request)
    return response


@app.get("/")
def root():
    return {"message": "Welcome to Sing-a-Song API..."}


app.include_router(router=users.router, prefix="/users")
app.include_router(router=songs.router, prefix="/songs")
app.include_router(router=comments.router, prefix="/comments")