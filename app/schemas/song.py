from .PyObjectId import PyObjectId
from pydantic import BaseModel, Field
from bson import ObjectId
from typing import Optional, List
import datetime


class InsertComment(BaseModel):
    text: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "text": "Greate tab. Thanks!",
            }
        }


class AcknowledgedMessage(BaseModel):
    message: str = Field(...)
    acknowledged: bool = Field(...)


class Comment(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    author_id: PyObjectId = Field(default_factory=PyObjectId, alias="author_id")
    author_username: str = Field(...)
    text: str = Field(...)
    likes: int = 0
    liked_by: List[PyObjectId] = []
    inserted_at: datetime.datetime = datetime.datetime.utcnow()

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "author_id": "62500f3c6eae53a72a4a49d2",
                "text": "Greate tab. Thanks!",
                "likes": 0,
                "liked_by": [],
                "inserted_at": "2023-05-05T10:10:14.504000"
            }
        }


class InsertSong(BaseModel):
    title: str = Field(...)
    artist: str = Field(...)
    text: str = Field(...)
    difficulty: Optional[int] = Field(...)
    capo: Optional[int] = Field(...)
    tuning: Optional[list[str]] = Field(...)
    key: Optional[str] = Field(...)
    chords: Optional[List[str]] = Field(...)
    author_id: PyObjectId = Field(default_factory=PyObjectId, alias="author_id")
    author_username: str | None = None
    inserted_at: datetime.datetime = datetime.datetime.utcnow()

    class Config:
        schema_extra = {
            "example": {
                "title": "Wonderwall",
                "artist": "Oasis",
                "text": "Today is gonna be the day That they're gonna throw it back to you By now you should've somehow Realized what you gotta do I don't believe that anybody Feels the way I do about you now Backbeat, the word is on the street That the fire in your heart is out I'm sure you've heard it all before But you never really had a doubt I don't believe that anybody feels The way I do about you now And all the roads we have to walk are winding And all the lights that lead us there are blinding There are many things that I would Like to say to you But I don't know how Because maybe You're gonna be the one that saves me And after all You're my wonderwall Today was gonna be the day But they'll never throw it back to you By now you should've somehow Realized what you're not to do I don't believe that anybody Feels the way I do About you now all the roads that lead you there were winding And all the lights that light the way are blinding There are many things that I would like to say to you But I don't know how I said maybe You're gonna be the one that saves me And after all You're my wonderwall I said maybe (I said maybe) You're gonna be the one that saves me And after all You're my wonderwall I said maybe (I said maybe) You're gonna be the one that saves me (saves me) You're gonna be the one that saves me (saves me) You're gonna be the one that saves me (saves me)",
                "difficulty": 2,
                "capo": 2,
                "tuning": [
                    "E",
                    "B",
                    "G",
                    "D",
                    "A",
                    "E"
                ],
                "key": "F#m",
                "chords": [
                    "Em",
                    "G",
                    "D",
                    "A7sus4",
                    "C"
                ]
            }
        }


class Song(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    title: str = Field(...)
    artist: str = Field(...)
    rating: int | None = None
    hits: int = 0
    times_rated: int | None = None
    likes: int = Field(...)
    liked_by: List[PyObjectId] = []
    difficulty: Optional[int] = Field(...)
    author_id: PyObjectId = Field(default_factory=PyObjectId, alias="author_id")
    author_username: str = Field(...)
    inserted_at: datetime.datetime = datetime.datetime.utcnow()

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "title": "Wonderwall",
				"artist": "Oasis",
                "rating": 5,
                "times_rated": 24,
                "difficulty": 2,
                "author_id": "62500f3c6eae53a72a4a49d2",
                "author_username": "johndoe",
                "inserted_at": "2023-05-05T10:10:14.504000"
            }
        }


class SongDetail(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    title: str = Field(...)
    artist: str = Field(...)
    text: str = Field(...)
    rating: int | None = None
    times_rated: int | None = None
    hits: int = 0
    likes: int = 0
    liked_by: List[PyObjectId] = []
    difficulty: Optional[int] = Field(...)
    capo: Optional[int] = Field(...)
    tuning: Optional[list[str]] = Field(...)
    key: Optional[str] = Field(...)
    chords: Optional[List[str]] = Field(...)
    author_id: PyObjectId = Field(default_factory=PyObjectId, alias="author_id")
    author_username: str = Field(...)
    inserted_at: datetime.datetime = datetime.datetime.utcnow()
    comments: List[Comment] = []

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "title": "Wonderwall",
				"artist": "Oasis",
                "text": "Em       G\nToday is gonna be the day\n\t\t\tD                  A7sus4\nThat they're gonna throw it back to you",
                "rating": 5,
                "times_rated": 24,
                "difficulty": 2,
                "capo": 2,
                "tuning": ["E", "B", "G", "D", "A", "E"],
                "key": "F#m",
                "chords": ["Em", "G", "D", "A7sus4", "C"],
                "author_id": "62500f3c6eae53a72a4a49d2",
                "author_username": "johndoe",
                "inserted_at": "2023-05-05T10:10:14.504000",
                "comments": []
            }
        }

class SongsResponseModel(BaseModel):
    data: List[Song] = Field(...)
    total: int = Field(...)
    count: int = Field(...)
    search_query: Optional[str] = Field(...)
    pagination: dict = Field(...)

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}