from .PyObjectId import PyObjectId
from pydantic import BaseModel, Field
from bson import ObjectId

class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None


class User(BaseModel):
    id: PyObjectId = Field(default_factory=PyObjectId, alias="_id")
    username: str
    email: str | None = None
    full_name: str | None = None

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        json_encoders = {ObjectId: str}
        schema_extra = {
            "example": {
                "username": "johndoe",
                "email": "john@example.com",
                "full_name": "John Doe"
            }
        }


class InsertUser(BaseModel):
    username: str
    email: str | None = None
    full_name: str | None = None
    password: str

    class Config:
        schema_extra = {
            "example": {
                "username": "johndoe",
                "email": "john@example.com",
                "full_name": "John Doe",
                "password": "secret"
            }
        }


class InsertUserHashed(BaseModel):
    username: str
    email: str | None = None
    full_name: str | None = None
    hashed_password: str


class UserInDB(User):
    hashed_password: str

    class Config:
        schema_extra = {
            "example": {
                "username": "johndoe",
                "email": "john@example.com",
                "full_name": "John Doe",
                "hashed_password": "$2b$12$MhXPjw4hrfOXMsu2SDdIv.I15eYM1BwxpXFp/lKkLLPCCz9Qp0YO6"
            }
        }