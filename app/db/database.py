import motor.motor_asyncio
import settings
import logging


logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(name)s:%(message)s',
    level=logging.INFO
    )


class Database():

    def __init__(self) -> None:
        self.client = None
        self.connection = None


    async def create_connection(self) -> None:
        if  self.client is None:
            self.client = motor.motor_asyncio.AsyncIOMotorClient(settings.MONGO_URI_ATLAS)
            self.connection = self.client[settings.DATABASE]
            
            try:
                db_info = await self.client.server_info()
                logging.info(f"MongoDB version: {db_info['version']}")
                logging.info("Connection successful...")
            except Exception as e:
                logging.exception(f"Error while connecting to database: {str(e)}")

    
    async def close_connection(self) -> None:
        self.client.close()
        logging.info("Database connection closed successfully...")