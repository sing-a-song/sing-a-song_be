from fastapi import APIRouter, Request, Depends, HTTPException
from http import HTTPStatus
from schemas import song as song_schema
from schemas import user as user_schema
from .users import get_current_user
from bson import ObjectId, errors
from helpers.pagination import get_pagination 
import datetime


router = APIRouter(tags=["songs"])


@router.get("/liked-songs",
            response_description="List of all songs liked by given user",
            response_model=song_schema.SongsResponseModel
            )
async def get_liked_songs(request: Request, page_num: int = 1, page_size: int = 10, search_query: str = None, current_user: user_schema.User = Depends(get_current_user)):
    page_size = page_size if page_size < 20 else 20

    start = (page_num - 1) * page_size
    end = start + page_size

    if search_query:
        data = await request.state.db["songs"].find({
            '$and': [
                {'liked_by': current_user.id},
                {
                    '$or': [
                        {
                            'title': {
                                '$regex': search_query, '$options': 'i'
                            }
                        },
                        {
                            'artist': {
                                '$regex': search_query, '$options': 'i'
                            }
                        }
                    ]
                }
            ]
        }).skip(start).limit(page_size).to_list(100)
    
        data_length = await request.state.db["songs"].count_documents({
            '$and': [
                {'liked_by': current_user.id},
                {
                    '$or': [
                        {
                            'title': {
                                '$regex': search_query, '$options': 'i'
                            }
                        },
                        {
                            'artist': {
                                '$regex': search_query, '$options': 'i'
                            }
                        }
                    ]
                }
            ]
        })

    else:
        data = await request.state.db["songs"].find({'liked_by': current_user.id}).skip(start).limit(page_size).to_list(100)
        data_length = await request.state.db["songs"].count_documents({'liked_by': current_user.id})

    result = song_schema.SongsResponseModel(
        data=data,
        total=data_length,
        count=page_size,
        search_query=search_query,
        pagination=get_pagination(end=end, page_num=page_num, page_size=page_size, data_length=data_length, search_query=search_query)
    )

    return result

@router.get("/my-songs",
            response_description="List of all songs created by given user",
            response_model=song_schema.SongsResponseModel
            )
async def get_user_songs(request: Request, page_num: int = 1, page_size: int = 10, search_query: str = None, current_user: user_schema.User = Depends(get_current_user)):
    page_size = page_size if page_size < 20 else 20

    start = (page_num - 1) * page_size
    end = start + page_size

    if search_query:
        data = await request.state.db["songs"].find({
            '$and': [
                {'author_id': current_user.id},
                {
                    '$or': [
                        {
                            'title': {
                                '$regex': search_query, '$options': 'i'
                            }
                        },
                        {
                            'artist': {
                                '$regex': search_query, '$options': 'i'
                            }
                        }
                    ]
                }
            ]
        }).skip(start).limit(page_size).to_list(100)

        data_length = await request.state.db["songs"].count_documents({
            '$and': [
                {'author_id': current_user.id},
                {
                    '$or': [
                        {
                            'title': {
                                '$regex': search_query, '$options': 'i'
                            }
                        },
                        {
                            'artist': {
                                '$regex': search_query, '$options': 'i'
                            }
                        }
                    ]
                }
            ]
        })

    else:
        data = await request.state.db["songs"].find({'author_id': current_user.id}).skip(start).limit(page_size).to_list(100)
        data_length = await request.state.db["songs"].count_documents({'author_id': current_user.id})


    result = song_schema.SongsResponseModel(
        data=data,
        total=data_length,
        count=page_size,
        search_query=search_query,
        pagination=get_pagination(end=end, page_num=page_num, page_size=page_size, data_length=data_length, search_query=search_query)
    )

    return result


@router.get("/",
            response_description="List all songs in database",
            response_model=song_schema.SongsResponseModel
            )
async def get_songs(request: Request, page_num: int = 1, page_size: int = 10, search_query: str = None):
    page_size = page_size if page_size < 20 else 20

    start = (page_num - 1) * page_size
    end = start + page_size

    if search_query:
        search_query_result = await request.state.db["songs"].find({
            '$or': [{
                'title': {
                    '$regex': search_query, '$options': 'i'
                }
            }, {
                'artist': {
                    '$regex': search_query, '$options': 'i'
                }
            }]
        }).skip(start).limit(page_size).to_list(100)

        data_length = await request.state.db["songs"].count_documents({
            '$or': [{
                'title': {
                    '$regex': search_query, '$options': 'i'
                }
            }, {
                'artist': {
                    '$regex': search_query, '$options': 'i'
                }
            }]
        })
        data = search_query_result
    else:
        data = await request.state.db["songs"].find().skip(start).limit(page_size).to_list(100)
        data_length = await request.state.db["songs"].estimated_document_count({})

    result = song_schema.SongsResponseModel(
        data=data,
        total=data_length,
        count=page_size,
        search_query=search_query,
        pagination=get_pagination(end=end, page_num=page_num, page_size=page_size, data_length=data_length, search_query=search_query)
    )

    return result


@router.get("/{song_id}",
            response_model=song_schema.SongDetail
            )
async def get_song_by_id(request: Request, song_id: str):
    try:
        result = await request.state.db["songs"].find_one({"_id": ObjectId(song_id)})

        if not result:
            raise HTTPException(
                status_code=404, detail="Song with given ID does not exists.")

        return result
    except errors.InvalidId:
        raise HTTPException(status_code=404, detail="Invalid ID.")


@router.post("/",
             response_model=song_schema.SongDetail,
             status_code=HTTPStatus.CREATED
             )
async def insert_new_song(request: Request, data: song_schema.InsertSong, current_user: user_schema.User = Depends(get_current_user)):

    new_song = song_schema.SongDetail(
        title=data.title,
        artist=data.artist,
        text=data.text,
        rating=None,
        times_rated=0,
        hits=0,
        likes=0,
        liked_by=[],
        difficulty=data.difficulty,
        capo=data.capo,
        tuning=data.tuning,
        key=data.key,
        chords=data.chords,
        author_id=current_user.id,
        author_username = current_user.username,
        inserted_at=datetime.datetime.utcnow(),
        comments=[]
    )

    inserted_song = await request.state.db["songs"].insert_one(new_song.dict())
    result = await request.state.db["songs"].find_one({"_id": inserted_song.inserted_id})
    return result

@router.put("/{song_id}",
            response_model=song_schema.AcknowledgedMessage
            )
async def like_song(song_id: str, request: Request, current_user: user_schema.User = Depends(get_current_user)):
    try:
        if (song := await request.state.db["songs"].find_one({"_id": ObjectId(song_id)})) is not None:
            
            if current_user.id in song["liked_by"]:
                 raise HTTPException(
                    status_code=403, detail="Song was already liked.")
        
            liked_song = await request.state.db["songs"].update_one(
                {
                    "_id": ObjectId(song_id)
                },
                {
                    "$inc": {
                        "likes": 1
                    },
                    "$push": {
                        "liked_by": current_user.id
                    }
                }
            )

            result = song_schema.AcknowledgedMessage(
                message="Song liked",
                acknowledged=liked_song.acknowledged
            )

            return result
        else:
            raise HTTPException(
                status_code=404, detail="Song with given ID does not exists.")
    except errors.InvalidId:
        raise HTTPException(status_code=404, detail="Invalid ID.")