from datetime import datetime, timedelta
from fastapi import Depends, APIRouter, HTTPException, Request, status
from fastapi.security import OAuth2PasswordRequestForm
from jose import jwt, JWTError
from schemas import user as user_schema
from auth import auth
import settings
import logging


logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(name)s:%(message)s',
    level=logging.INFO
    )

router = APIRouter(tags=["users"])


async def get_user(db, username: str):
    if (user := await db["users"].find_one({"username": username})) is not None:
        return user_schema.UserInDB(**user)


async def authenticate_user(db, username: str, password: str):
    user = await get_user(db, username)
    if not user:
        return False
    if not auth.verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt


async def get_current_user(request: Request, token: str = Depends(auth.oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = user_schema.TokenData(username=username)
    except JWTError:
        raise credentials_exception
    user = await get_user(request.state.db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user


@router.post("/signup", response_model=user_schema.UserInDB)
async def signup_user(request: Request, new_user_data: user_schema.InsertUser):
    new_user_data = user_schema.InsertUserHashed(
        username=new_user_data.username, 
        email=new_user_data.email, 
        full_name=new_user_data.full_name,
        hashed_password=auth.get_password_hash(new_user_data.password)
    )

    if await request.state.db["users"].find_one({"username": new_user_data.username}) is not None:
        raise HTTPException(status_code=500, detail=f"User with entered username already exists")

    try:    
        new_user = await request.state.db["users"].insert_one(new_user_data.dict())
        inserted_user = await request.state.db["users"].find_one({"_id": new_user.inserted_id})
        return user_schema.UserInDB(**inserted_user)
    except Exception as e:
        logging.exception(f"Error while creating new user: {e}")
        raise HTTPException(status_code=500, detail=f"Error while creating new user: {e}")


@router.post("/login", response_model=user_schema.Token)
async def login_for_access_token(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    user = await authenticate_user(request.state.db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.get("/me/", response_model=user_schema.User)
async def read_users_me(current_user: user_schema.User = Depends(get_current_user)):
    return current_user


@router.get("/me/items/")
async def read_own_items(current_user: user_schema.User = Depends(get_current_user)):
    return [{"item_id": "Foo", "owner": current_user.username}]