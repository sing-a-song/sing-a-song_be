from fastapi import APIRouter, Request, Depends, HTTPException
from schemas import song as song_schema
from schemas import user as user_schema
from .users import get_current_user
from bson import ObjectId, errors
import datetime


router = APIRouter(tags=["comments"])

@router.put("/comment/{song_id}/{comment_id}",
            response_model=song_schema.AcknowledgedMessage
            )
async def like_comment(song_id: str, comment_id: str, request: Request,  current_user: user_schema.User = Depends(get_current_user)):
    try:

        if (song := await request.state.db["songs"].find_one({"_id": ObjectId(song_id)})) is not None:
            try:
                liked_comment = [item for item in song["comments"] if item["id"] == ObjectId(comment_id)][0]
            except:
                raise HTTPException(
                    status_code=404, detail="Comment with given ID does not exists.")
            if current_user.id in liked_comment["liked_by"]:
                raise HTTPException(
                    status_code=403, detail="Comment was already liked.")
        else:
            raise HTTPException(
                status_code=404, detail="Song with given ID does not exists.")

        liked_comment = await request.state.db["songs"].update_one(
            {"_id": ObjectId(song_id), "comments.id": ObjectId(comment_id)},
            {
                "$inc": {
                    "comments.$.likes": 1
                }, 
                "$push": {
                    "comments.$.liked_by": current_user.id
                }
            }
        )

        if liked_comment.modified_count < 1:
            raise HTTPException(
                status_code=404, detail="Comment with given ID does not exists.")

        result = song_schema.AcknowledgedMessage(
            message="Comment liked",
            acknowledged=liked_comment.acknowledged
        )

        return result
    except errors.InvalidId:
        raise HTTPException(status_code=404, detail="Invalid ID.")


@router.post("/comment/{song_id}",
             response_model=song_schema.AcknowledgedMessage
             )
async def comment_on_song(song_id: str, request: Request, data: song_schema.InsertComment, current_user: user_schema.User = Depends(get_current_user)):
    try:
        comment = song_schema.Comment(
            author_id=current_user.id,
            author_username=current_user.username,
            text=data.text,
            # likes=0,
            inserted_at=datetime.datetime.utcnow()
        )

        inserted_comment = await request.state.db["songs"].update_one(
            {"_id": ObjectId(song_id)},
            {"$push": {"comments": comment.dict()}})

        if inserted_comment.modified_count < 1:
            raise HTTPException(
                status_code=404, detail="Song with given ID does not exists.")

        result = song_schema.AcknowledgedMessage(
            message="Comment added",
            acknowledged=inserted_comment.acknowledged
        )

        return result

    except errors.InvalidId:
        raise HTTPException(status_code=404, detail="Invalid ID.")
